#include "testcalibration.h"


void TestCalibration::init()
{
    calibrator.help();
}

void TestCalibration::cleanup()
{
    calibrator.stopCamera();
}

void TestCalibration::helpTest()
{
    QCOMPARE(QString("This is a camera calibration sample. \n Usage: calibration configurationFile \n Near the sample file you'll find the configuration file, which has detailed help of \n how to edit it.  It may be any OpenCV supported file format XML/YAML."), QString(calibrator.help()));
}

void TestCalibration::basicTest()
{
    somevar = 10;
    QCOMPARE(somevar + 5, 15);
}

// void TestCalibration::toUpper()
// {
//     QString str = "Hello";
//     QCOMPARE(str.toUpper(), QString("HELLO"));
// }

// full Qt application
//QTEST_MAIN(TestName)
// core Qt application: no GUI, but event loop is available
//QTEST_GUILESS_MAIN(TestName) 
// no Qt application: no GUI and no events
//QTEST_APPLESS_MAIN(TestCalibration)


// uncomment if in .cpp only
//#include "tmp/testcalibration.moc"