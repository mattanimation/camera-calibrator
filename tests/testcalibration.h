#include <QtTest/QtTest>

#include "calibrator.h"

class TestCalibration: public QObject
{
    Q_OBJECT
private slots:

    // -- setup / cleanup ---
    // built in test methods
    //void initTestCase();
    // called before every test function
    void init();
    // called after every test function
    void cleanup();
    // called after the last test function
    //void cleanupTestCase();

    // -- tests --
    void helpTest();
    void basicTest();
private:
    uint8_t somevar = 0;

private:
    Calibrator calibrator;

};
