# Camera Calibrator

A standalone camera calibration tool written in c++, opencv and QT.

[![Calibrator Demonstration](http://img.youtube.com/vi/d2akwDCo7o4/0.jpg)](https://www.youtube.com/watch?v=d2akwDCo7o4 "Calibrator Demonstration")

## Users

### Supported / Tested OS
* linux_x64 (_PopOS! / Ubuntu 18.04_)

For instructions on use see [the docs](docs/README.md)


## Developers

See information below

### Dependencies
* QT5
* opencv4

### Build
* `qmake`
* `make`

### Run
* `./build/CameraCalibrator`

### Test
* cd to `src` folder
* `qmake -config test_conf`

### Verify
* make checksum: `md5sum -c CameraCalibrator_v0.1.0_linux-x64.zip > CameraCalibrator_v0.1.0_linux-x64.md5`
* validate: `md5sum -c <<< $(echo $(cat CameraCalibrator_v0.1.0_linux-x64.md5)  CameraCalibrator_v0.1.0_linux-x64.zip)`

## Instructions
see the [Docs](docs/README.md)


## TODO
* [ ] - expose the config as ui elements
* [x] - add camera select dropdown

---

## Resources

Links to things that were useful while building this tool

### QT related
* https://doc.qt.io/qt-5/qmake-variable-reference.html
* https://amin-ahmadi.com/2018/03/29/how-to-read-process-and-display-videos-using-qt-and-opencv/
* https://bitbucket.org/amahta/videoprocessor/src/master/
* use cmake: http://amin-ahmadi.com/2018/02/23/how-to-use-cmake-with-qt5-and-opencv-3-projects/
* adding cv libs to qmake: http://amin-ahmadi.com/2018/03/18/adding-required-includes-and-libs-for-opencv-3-4-1-in-qmake-projects/
* https://stackoverflow.com/questions/29338250/qt-opencv-play-videos-with-stdthread
* https://stackoverflow.com/questions/4637594/how-to-get-a-list-video-capture-devices-names-web-cameras-using-qt-crossplatf
* http://zetcode.com/gui/qt5/widgets/


#### Testing in QT
* https://doc.qt.io/qt-5/qttestlib-tutorial1-example.html
* https://www.qtcentre.org/threads/46492-QtTest-use-it-in-your-application
* http://blog.davidecoppola.com/2017/11/cpp-unit-testing-with-qt-test-introduction/
* https://www.3dh.de/qt-automated-unit-tests-with-qmake/


### calibration resources:
* https://docs.opencv.org/2.4/doc/tutorials/calib3d/camera_calibration/camera_calibration.html#cameracalibrationopencv
* https://docs.opencv.org/2.4/doc/tutorials/core/file_input_output_with_xml_yml/file_input_output_with_xml_yml.html#fileinputoutputxmlyaml
* https://docs.opencv.org/2.4/doc/tutorials/calib3d/camera_calibration_square_chess/camera_calibration_square_chess.html
* https://www.learnopencv.com/camera-calibration-using-opencv/
* https://aishack.in/tutorials/calibrating-undistorting-opencv-oh-yeah/
* https://markhedleyjones.com/projects/calibration-checkerboard-collection
* https://stackoverflow.com/questions/4290834/how-to-get-a-list-of-video-capture-devices-web-cameras-on-linux-ubuntu-c 


