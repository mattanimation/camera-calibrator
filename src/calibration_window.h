#include "calibration_ui.h"
#include "calibrator.h"
#include "logger.h"

#include "opencv2/opencv.hpp"

#include "worker.h"

#include <thread>
#include <vector>

class QThread;

class MainUI : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainUI(QWidget *parent = 0);
    ~MainUI();

    //QProcess *proc;

    void init();

protected:
    void closeEvent(QCloseEvent *event);

signals:
    void startBtnPress(QString devicepath);
    void toggleCaptureBtnPress();
    void startCalibrationBtnPress();
    void unDistortBtnPress();

public slots:
    void updateError();
    void updateText();

private slots:
    void on_startCameraBtn_pressed();
    void on_toggleCaptureBtn_pressed();
    void on_startCalibrationBtn_pressed();
    void on_unDistortBtn_pressed();
    void displayFrame(cv::Mat frame);
    void addDetection(QString filepath);
    void calibrationResult(QString msg);
    void undistortResult(cv::Mat frame);

private:
    Ui::MainUI *ui;

    QGraphicsPixmapItem pixmap;
    std::vector<QGraphicsPixmapItem> pixlist;
    //Calibrator calibrator;
    //std::thread cameraWorker;
    QThread* threads[1];
    Worker* workers[1];

};