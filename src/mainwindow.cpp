#include "calibration_window.h"
#include "calibration_ui.h"

#include <QDebug>
#include <QThread>
#include <QLabel>
#include <QGridLayout>
#include <QTextStream>

//#include "mainwindow.h"
// #include "ui/ui_mainwindow.h"

MainUI::MainUI(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainUI)
{
    ui->setupUi(this);
    ui->graphicsView->setScene(new QGraphicsScene(this));
    ui->graphicsView->scene()->addItem(&pixmap);

    // ensures the signals and slots know about this data type
    qRegisterMetaType< cv::Mat >("cv::Mat");

    qDebug() << "Main thread " << QThread::currentThreadId();
    init();
}

MainUI::~MainUI()
{
    //cameraWorker.join();
    delete ui;
}


void MainUI::init(){


    //QObject *parent;
    //proc = new QProcess(parent);
    //proc->setProcessChannelMode(QProcess::ProcessChannelMode::MergedChannels);

    for (auto &&i : Calibrator::getCameraPaths())
    {
        ui->cameraSelectCombo->addItem(QString::fromStdString(i));
    }
    
    //ui->cameraSelectCombo->addItem(calibrator.getCameraPaths())

    // create new thread with worker to handle
    // sending frames back to ui from calibrator
    uint8_t i=0;
    threads[i] = new QThread;
    workers[i] = new Worker();
    workers[i]->moveToThread(threads[i]);

    //workers[i]->proc->setProcessChannelMode(QProcess::ProcessChannelMode::MergedChannels);

    connect(workers[i], SIGNAL(frameFinished(cv::Mat)), this, SLOT(displayFrame(cv::Mat)));
    connect(workers[i], SIGNAL(checkerDetected(QString)), this, SLOT(addDetection(QString)));
    connect(workers[i], SIGNAL(calibrationFinished(QString)), this, SLOT(calibrationResult(QString)));
    connect(workers[i], SIGNAL(undistortFinished(cv::Mat)), this, SLOT(undistortResult(cv::Mat)));

    // connect() actually returns a QMetaObject::Connection object,
    // which implicitly converts to bool.
    connect(this, &MainUI::startBtnPress, workers[i], &Worker::toggleCamera);
    connect(this, &MainUI::toggleCaptureBtnPress, workers[i], &Worker::toggleCapture);
    connect(this, &MainUI::startCalibrationBtnPress, workers[i], &Worker::runCalibration);
    connect(this, &MainUI::unDistortBtnPress, workers[i], &Worker::undistortFrame);
    // const bool connected = connect(sender, &Sender::aSignal, 
    //                             receiver, &Receiver::aSlot);
    //qDebug() << "Connection established?" << connected;

    connect(threads[i], SIGNAL(started()), workers[i], SLOT(readVideo()));

    //listen to stdout
    //connect(workers[i]->proc, SIGNAL(readyReadStdError()), this, SLOT(updateError()));
    //connect(workers[i]->proc, SIGNAL(readyReadStdOutput()), this, SLOT(updateText()));

    connect(workers[i], SIGNAL(finished()), threads[i], SLOT(quit()));
    connect(workers[i], SIGNAL(finished()), workers[i], SLOT(deleteLater()));
    connect(threads[i], SIGNAL(finished()), threads[i], SLOT(deleteLater()));

    threads[i]->start();
}

void MainUI::updateError()
{
    //QByteArray data = workers[0]->proc->readAllStandardError();
    //ui->stdText->append(QString(data));
}
 
void MainUI::updateText()
{
   //QByteArray data = workers[0]->proc->readAllStandardOutput();
   //ui->stdText->append(QString(data));
}

void MainUI::displayFrame(cv::Mat frame)
{
    QPixmap p = QPixmap::fromImage(QImage(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_RGB888).rgbSwapped());
    //p = p.scaled(QSize(frame.cols/2, frame.rows/2));
    //labels[index]->setPixmap(p);
    pixmap.setPixmap(p);
    ui->graphicsView->fitInView(&pixmap, Qt::KeepAspectRatio);
}

void MainUI::addDetection(QString filepath)
{
    std::cout << "DETECTION: adding " << filepath.toStdString() << " as image to listview " << "\n";
    //QString testPath = QString::fromStdString(filepath);
    //qDebug() << testPath;
    //QPixmap pixmap;
    //pixmap.load(QString::fromStdString(filepath));

    // QPixmap bgPixmap(filepath);
    // QPixmap scaled = bgPixmap.scaled( QSize(128, 128), Qt::KeepAspectRatio, Qt::SmoothTransformation );
    // QIcon icon(scaled);

    // add to image list now
    new QListWidgetItem(QIcon(filepath),
                        filepath,
                        ui->capturedListView);
    
    //newItem->setIcon();
    //ui->capturedListView->addItem(newItem);

}

void MainUI::calibrationResult(QString msg)
{
    QMessageBox::warning(this,
                         "Calibration Results",
                         msg);
}

void MainUI::undistortResult(cv::Mat uFrame)
{

    if(uFrame.empty())
    {
        QMessageBox::warning(this,
                             "Warning",
                             "Undistort failed... see console for more context.");
        return;
    }

    QMessageBox about;
    about.setText("Hopefully this looks right 🤞!");
    about.setStandardButtons(QMessageBox::Ok);
    about.setIconPixmap(QPixmap::fromImage(QImage(uFrame.data,
                                                       uFrame.cols,
                                                       uFrame.rows,
                                                       uFrame.step,
                                                       QImage::Format_RGB888).rgbSwapped()));
    about.setDefaultButton(QMessageBox::Ok);
    about.show();
    about.exec();
}


void MainUI::on_startCameraBtn_pressed()
{
    qDebug() << "selected camera: " << ui->cameraSelectCombo->currentText();
    emit startBtnPress(ui->cameraSelectCombo->currentText());
    if(QString::compare(ui->startCameraBtn->text(), "Stop Camera") == 0)
        ui->startCameraBtn->setText("Start Camera");
    else
        ui->startCameraBtn->setText("Stop Camera");

}

void MainUI::on_toggleCaptureBtn_pressed()
{
    emit toggleCaptureBtnPress();
    if(QString::compare(ui->toggleCaptureBtn->text(), "Stop Capture") == 0)
        ui->toggleCaptureBtn->setText("Start Capture");
    else
        ui->toggleCaptureBtn->setText("Stop Capture");

}

void MainUI::on_unDistortBtn_pressed()
{
    emit unDistortBtnPress();
}


void MainUI::on_startCalibrationBtn_pressed()
{
    emit startCalibrationBtnPress();
}

void MainUI::closeEvent(QCloseEvent *event)
{
    //cameraWorker.join();
    //calibrator.stopCamera();
    event->accept();
    
    // if(video.isOpened()){
    //     QMessageBox::warning(this,
    //                          "Warning",
    //                          "Stop the video before closing the application!");
    //     event->ignore();
    // }
    // else {
    //     event->accept();
    // }
}