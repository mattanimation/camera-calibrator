#pragma once

#include <iostream>
#include <string>
#include <map>

//the following are UBUNTU/LINUX, and MacOS ONLY terminal color codes.
// ref: https://stackoverflow.com/questions/9158150/colored-output-in-c/9158263
#define RESET_COLOR       "\033[0m"       /* resets color */
#define BLACK_COLOR       "\033[30m"      /* Black */
#define RED_COLOR         "\033[31m"      /* Red */
#define GREEN_COLOR       "\033[32m"      /* Green */
#define YELLOW_COLOR      "\033[33m"      /* Yellow */
#define BLUE_COLOR        "\033[34m"      /* Blue */
#define MAGENTA_COLOR     "\033[35m"      /* Magenta */
#define CYAN_COLOR        "\033[36m"      /* Cyan */
#define WHITE_COLOR       "\033[37m"      /* White */
#define BOLDBLACK_COLOR   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED_COLOR     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN_COLOR   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW_COLOR  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE_COLOR    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA_COLOR "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN_COLOR    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE_COLOR   "\033[1m\033[37m"      /* Bold White */


/**
 * Logger: a simple logger for stdout
 * */
class Logger
{
public:
    void Debug(std::string const &msg)
    {
        log(DEBUG, msg);
    }
    void Info(std::string const &msg)
    {
        log(INFO, msg);
    }
    void Warning(std::string const &msg)
    {
        log(WARNING, msg);
    }
    void Error(std::string const &msg)
    {
        log(ERROR, msg);
    }
    void Critical(std::string const &msg)
    {
        log(CRITICAL, msg);
    }
    void setLogLevel(uint8_t lvl)
    {
        m_lvl = lvl;
    }

private:
    struct LogLevelItem {
        const std::string name;
        const std::string color;
    };
    time_t timenow = time(NULL);
    void log(uint8_t level, std::string const &msg)
    {
        if (level <= m_lvl)
        {
            std::cout << m_logMap.at(level).color << m_logMap.at(level).name << RESET_COLOR << " > "  << YELLOW_COLOR << "CC: " << RESET_COLOR << BOLDWHITE_COLOR << msg << RESET_COLOR << " @ " << ctime(&timenow);
        }
    }
    enum log_level
    {
        NONE = 0,
        CRITICAL = 1,
        ERROR = 2,
        WARNING = 3,
        INFO = 4,
        DEBUG = 5,
    };
    uint8_t m_lvl = 3;  // warning by default
    std::map<uint8_t, LogLevelItem> m_logMap{
        std::make_pair(0, LogLevelItem{.name="NONE", .color=BLACK_COLOR}),
        std::make_pair(1, LogLevelItem{.name="😱 CRITICAL 💥", .color=BOLDRED_COLOR}),
        std::make_pair(2, LogLevelItem{.name="😵 ERROR 🔥", .color=RED_COLOR}),
        std::make_pair(3, LogLevelItem{.name="😨 WARNING", .color=BOLDYELLOW_COLOR}),
        std::make_pair(4, LogLevelItem{.name="😐 INFO", .color=CYAN_COLOR}),
        std::make_pair(5, LogLevelItem{.name="😑 DEBUG", .color=GREEN_COLOR})
    };
};