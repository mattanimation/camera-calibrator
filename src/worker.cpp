#include "worker.h"

#include <QDebug>
#include <QThread>
#include <QTime>

Worker::Worker()
{
    //proc = new QProcess{this};
    isAlive = true;
}

Worker::~Worker()
{
    isAlive = false;
    calibrator.stopCamera();
}

void Worker::toggleCamera(QString devicepath)
{
    if(!calibrator.isActive)
        calibrator.startCamera(devicepath.toStdString());
    else
        calibrator.stopCamera();
}

void Worker::toggleCapture()
{
    if(!calibrator.isCapturing)
        calibrator.startCapture();
    else
        calibrator.stopCapture();
}

void Worker::undistortFrame(){
    // make sure the camera calibration params are in memory
    cv::Mat uFrame;
    if(calibrator.readCameraParams())
    {
        cv::Mat currFrame = calibrator.getCameraFrame();
        uFrame = calibrator.undistortFrame(currFrame);
    }
    emit undistortFinished(uFrame);
}


void Worker::runCalibration()
{
    try{
        calibrator.handleCalibration();
        emit calibrationFinished(QString::fromStdString("Calibration Complete! The Camera params are located at: " + calibrator.settings.outputFileName));
    }catch(std::exception &e)
    {
        std::cerr << e.what() << "\n";
        emit calibrationFinished(QString::fromStdString("Calibration Failed, see console for errors..."));
    }
}

void Worker::readVideo()
{
    cv::Mat frame;
    while(isAlive)
    {
        if(calibrator.isActive)
        {
            frame = calibrator.getCameraFrame();
            if (frame.empty())
            {
                frame = cv::Mat(cv::Size(720, 576), CV_8UC3, cv::Scalar(192, 0, 0));
                emit frameFinished(frame);
                break;
            }

            if(calibrator.isCapturing){
                Calibrator::DetectionResult result = calibrator.processFrame(frame);
                if(result.found){
                    //emit path to file to load image that was found
                    emit checkerDetected(QString::fromStdString(result.filepath));
                    //break;
                }
            }

            emit frameFinished(frame.clone());
            QThread::msleep(30);

        }
        else{
            //just hang out until camera is on
            QThread::msleep(500);
        }
        qApp->processEvents();
    }

    emit finished();
}