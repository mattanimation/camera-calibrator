#pragma once


#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtWidgets/QMessageBox>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>
#include <QtGui/QtEvents>


QT_BEGIN_NAMESPACE

class CalibrationUI
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout;
    QHBoxLayout *horizontalLayout;
    QHBoxLayout *helpHorizLayout;
    //QLabel *label;
    QLabel *helpLabel;
    //QLineEdit *videoEdit;
    QComboBox *cameraSelectCombo;
    QPushButton *startCameraBtn;
    QPushButton *toggleCaptureBtn;
    QPushButton *startCalibrationBtn;
    QPushButton *unDistortBtn;
    QGraphicsView *graphicsView;
    QListWidget *capturedListView;
    QTextEdit *stdText;
    QHBoxLayout *stdLayout;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1280, 720);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout = new QGridLayout(centralWidget);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        
        //label = new QLabel(centralWidget);
        //label->setObjectName(QStringLiteral("label"));

        //horizontalLayout->addWidget(label);

        //add help text
        helpHorizLayout = new QHBoxLayout();
        helpHorizLayout->setSpacing(6);
        helpHorizLayout->setObjectName(QStringLiteral("helpHorizLayout"));
        helpLabel = new QLabel(centralWidget);
        helpLabel->setObjectName(QStringLiteral("HelpLabel"));
        helpHorizLayout->addWidget(helpLabel);

        //videoEdit = new QLineEdit(centralWidget);
        //videoEdit->setObjectName(QStringLiteral("videoEdit"));

        //horizontalLayout->addWidget(videoEdit);

        //camera select
        //QStringList cameraList = {"/dev/video0", "/dev/video1", "/dev/video2", "/dev/video3"};
        cameraSelectCombo = new QComboBox(centralWidget);
        //cameraSelectCombo->addItems(cameraList);

        startCameraBtn = new QPushButton(centralWidget);
        startCameraBtn->setObjectName(QStringLiteral("startCameraBtn"));

        toggleCaptureBtn = new QPushButton(centralWidget);
        toggleCaptureBtn->setObjectName(QStringLiteral("toggleCaptureBtn"));

        startCalibrationBtn = new QPushButton(centralWidget);
        startCalibrationBtn->setObjectName(QStringLiteral("startCalibrationBtn"));

        unDistortBtn = new QPushButton(centralWidget);
        unDistortBtn->setObjectName(QStringLiteral("unDistortBtn"));

        horizontalLayout->addWidget(cameraSelectCombo);
        horizontalLayout->addWidget(startCameraBtn);
        horizontalLayout->addWidget(toggleCaptureBtn);
        horizontalLayout->addWidget(startCalibrationBtn);
        horizontalLayout->addWidget(unDistortBtn);

        gridLayout->addLayout(horizontalLayout, 1, 0, 1, 1);
        gridLayout->addLayout(helpHorizLayout, 2, 0, 1, 1);

        // camera view
        graphicsView = new QGraphicsView(centralWidget);
        graphicsView->setObjectName(QStringLiteral("graphicsView"));

        gridLayout->addWidget(graphicsView, 0, 0, 1, 1);

        // captured image side list
        capturedListView = new QListWidget(centralWidget);
        capturedListView->setObjectName(QStringLiteral("capturedListView"));
        capturedListView->setIconSize(QSize(128,128));
        gridLayout->addWidget(capturedListView, 0, 1, 0.5, 1);

        //std output text view
        stdLayout = new QHBoxLayout();
        stdLayout->setSpacing(6);
        stdLayout->setObjectName(QStringLiteral("stdLayout"));
        stdText = new QTextEdit(centralWidget);
        stdLayout->addWidget(stdText);

        gridLayout->addLayout(stdLayout, 3, 0, 0.5, 1);

        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        QString appver = QString("Camera Calibrator v");
        QApplication::setApplicationVersion(APP_VERSION);
        appver.append(QApplication::applicationVersion());
        MainWindow->setWindowTitle(appver); //QApplication::translate("MainWindow", "", Q_NULLPTR));
        //label->setText(QApplication::translate("MainWindow", "File Path / RTSP Url / Camera Index :", Q_NULLPTR));
        helpLabel->setText(QApplication::translate("MainWindow", "Make sure you have the the configuration file, which has detailed help of how to edit it.  It may be any OpenCV supported file format XML/YAML.", Q_NULLPTR));
        startCameraBtn->setText(QApplication::translate("MainWindow", "Start Camera", Q_NULLPTR));
        toggleCaptureBtn->setText(QApplication::translate("MainWindow", "Start Capture", Q_NULLPTR));
        startCalibrationBtn->setText(QApplication::translate("MainWindow", "Run Calibration", Q_NULLPTR));
        unDistortBtn->setText(QApplication::translate("MainWindow", "UnDistort Test", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainUI: public CalibrationUI {};
} // namespace Ui

QT_END_NAMESPACE

