#include "calibrator.h"

#include <time.h>
#include <ctime>
#include <string.h>

#include <chrono>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>




Calibrator::Calibrator(){
    // sets logging to debug level
    // should this be an env var?
    logger.setLogLevel(5);
    logger.Info("🥳 Calibrator v" + m_version);

    isActive = false;
    isCapturing = false;
    lastFoundTime = std::chrono::milliseconds(0);


    help();
    // TODO change to yml cause xml is yucky
    // read in the settings
    bool couldRead = readSettings("default.xml");
    if(!couldRead) {
        logger.Error("Failed to read config, stopping");
        return;
    }

    //create calibration image directory
    //char *homedir;
    //if ((homedir = getenv("HOME")) == NULL) {
    //    homedir = getpwuid(getuid())->pw_dir;
    //}
    //calibrationImageDirectory = strcat(homedir, "/calibration_images/");
    calibrationImageDirectory = "calibration_images/";
    if(mkdir(calibrationImageDirectory, 0777) == -1){
        logger.Warning("Failed to make directory: " + std::string(calibrationImageDirectory));
        std::cerr << "Error" << strerror(errno) << "\n";
    }
    else{
        logger.Info("Made directory: " + std::string(calibrationImageDirectory));
    }

    // example saving out the settings
    // bool couldWrite = writeSettings("default.yml");
    // if(!couldWrite) {
    //     logger.Error("Failed to write config, stopping");
    //     return;
    // }


}

Calibrator::~Calibrator(){
    stopCapture();
    stopCamera();
    logger.Info("🥺 Killed calibrator...");
}

bool Calibrator::startCamera(std::string devicepath)
{
    logger.Info("🎥 Starting Camera");
    if(video.isOpened()){
        stopCamera();
    }

    if(!video.open(devicepath)) //settings.cameraID))
    {
        logger.Warning("Was unable to open the camera "+ settings.cameraID);
        return false;
    }
    // TODO - get theses values from setttings? or GUI? - maybe dynamically
    video.set(cv::CAP_PROP_FRAME_WIDTH, 640);
    video.set(cv::CAP_PROP_FRAME_HEIGHT, 480);

    // reset the capture variables
    m_imagePoints = std::vector<std::vector<cv::Point2f>>{};

    isActive = true;
    return true;
}

void Calibrator::stopCamera()
{
    logger.Info("🎥 Stopping Camera");
    try
    {
        video.release();
        isActive = false;
    }
    catch(cv::Exception err){
        logger.Critical("Failed to close the camera device: " + err.msg);
        std::cout << err.code << " - "<< err.msg << "\n";
        return;
    }
}

void Calibrator::startCapture()
{
    if(isActive)
    {
        if(!isCapturing) { isCapturing = true; }
    }
}

void Calibrator::stopCapture()
{
    if(isCapturing) { isCapturing = false; }
}

cv::Mat Calibrator::getCameraFrame()
{
    //logger.Debug("Grabbing current camera frame");
    cv::Mat result;
    if( video.isOpened() )
    {
        video >> result;
    }
    return result;
}

Calibrator::DetectionResult Calibrator::processFrame(cv::Mat& frame)
{
    Calibrator::DetectionResult result{found:false, filepath:""};

    int mode = settings.inputType == Settings::IMAGE_LIST ? CAPTURING : DETECTION;
    clock_t prevTimestamp = 0;
    const cv::Scalar RED(0,0,255), GREEN(0,255,0);
    //const char ESC_KEY = 27;

    bool blinkOutput = false;

    //frame = nextImage();

    // TODO (station) - should we use this? and have it auto calibrate after n frames captured?
    //-----  If no more image, or got enough, then stop calibration and show result -------------
    // if( mode == CAPTURING && m_imagePoints.size() >= (unsigned)settings.nrFrames )
    // {
    //     logger.Info("Got enough points captured");
    //     if( runCalibrationAndSave(settings, m_imageSize, m_cameraMatrix, m_distCoeffs, m_imagePoints))
    //         mode = CALIBRATED;
    //     else
    //         mode = DETECTION;
    // }
    // if(frame.empty())          // If no more images then run calibration, save and stop loop.
    // {
    //     if( m_imagePoints.size() > 0 )
    //         runCalibrationAndSave(settings, m_imageSize, m_cameraMatrix, m_distCoeffs, m_imagePoints);
    //     return result;
    // }

    m_imageSize = frame.size();  // Format input image.
    if( settings.flipVertical )
        flip( frame, frame, 0 );
    
    // make grayscale
    cv::cvtColor(frame, m_greyFrame, cv::COLOR_BGR2GRAY);

    std::vector<cv::Point2f> pointBuf;

    bool found;
    switch( settings.calibrationPattern ) // Find feature points on the input format
    {
    case Settings::CHESSBOARD:
        //logger.Debug("Checking for a chessboard...");
        found = cv::findChessboardCorners(frame, settings.boardSize, pointBuf,
                                          cv::CALIB_CB_ADAPTIVE_THRESH | cv::CALIB_CB_FAST_CHECK | cv::CALIB_CB_NORMALIZE_IMAGE);
        break;
    case Settings::CIRCLES_GRID:
        found = cv::findCirclesGrid( frame, settings.boardSize, pointBuf );
        break;
    case Settings::ASYMMETRIC_CIRCLES_GRID:
        found = cv::findCirclesGrid( frame, settings.boardSize, pointBuf, cv::CALIB_CB_ASYMMETRIC_GRID );
        break;
    default:
        found = false;
        break;
    }


    std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now().time_since_epoch());
    std::chrono::milliseconds msDelta = std::chrono::duration_cast< std::chrono::milliseconds >(ms - lastFoundTime);

    //std::cout << "ms: " << ms.count() << "  last: " << lastFoundTime.count() << "  delta: " << msDelta.count() << "\n";
    //only capture every 3 seconds if found
    //TODO look at where points are in frame and try and 
    // derive a way to only capture if points are in a specific orientation
    // to get a good range of captures
    if (found && (ms.count() > lastFoundTime.count() + foundWaitTime))
    {
        logger.Info("Found a calibration pattern!");
        // improve the found corners' coordinate accuracy for chessboard
        if( settings.calibrationPattern == Settings::CHESSBOARD)
        {
            //logger.Debug("It was a chessboard, improving corners...");
            cornerSubPix( m_greyFrame, pointBuf, cv::Size(11,11),
                cv::Size(-1,-1), cv::TermCriteria( cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 30, 0.001 )); //0.1 or 0.001? I set in middle
        }

        //if( mode == CAPTURING &&  // For camera only take new samples after delay time
        //    (!video.isOpened() || clock() - prevTimestamp > settings.delay * 1e-3 * CLOCKS_PER_SEC) )
        //{
        //    m_imagePoints.push_back(pointBuf);
        //    prevTimestamp = clock();
        //    blinkOutput = video.isOpened();
        //}
        m_imagePoints.push_back(pointBuf);
        
        //SAVE THE IMAGE
        result = saveCapturedImage(frame);

        // Draw the corners for visual feedback
        drawChessboardCorners(frame, settings.boardSize, cv::Mat(pointBuf), found );

        lastFoundTime = ms;

    }
    // else{
    //     logger.Debug("sorry old chap, no checkerboard this frame...");
    // }

    return result;

    //----------------------------- Output Text ------------------------------------------------
    /*
    std::string msg = (mode == CAPTURING) ? "100/100" :
                mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
    int baseLine = 0;
    cv::Size textSize = cv::getTextSize(msg, 1, 1, 1, &baseLine);
    cv::Point textOrigin(frame.cols - 2*textSize.width - 10, frame.rows - 2*baseLine - 10);

    if( mode == CAPTURING )
    {
        if(settings.showUndistorsed)
            msg = cv::format( "%d/%d Undist", (int)imagePoints.size(), settings.nrFrames );
        else
            msg = cv::format( "%d/%d", (int)imagePoints.size(), settings.nrFrames );
    }

    cv::putText( frame, msg, textOrigin, 1, 1, mode == CALIBRATED ?  GREEN : RED);

    if( blinkOutput )
        bitwise_not(frame, frame);
    */

}


// used if not working alongside the UI
bool Calibrator::startCaptureOrProcessImages()
{
    logger.Info("Starting capture or process.");
    uint8_t captureCount = settings.nrFrames;  //set to capture 25 images
    uint8_t amountCaptured = 0;

    if(isActive)
    {
        logger.Debug("is indeed active...");
        isCapturing = true;

        logger.Debug("Should be in the while loop? " + std::to_string(amountCaptured < captureCount && isCapturing));

        // handle camera frames
        cv::Mat view;
        view == nextImage();
        while(amountCaptured < captureCount && isCapturing)
        {
            logger.Debug("Processing Frame");
            processFrame(view);
        }
        return true;
    }
    else {
        // handle images in folder
        isCapturing = true;
        return true;
    }

    return false;
}


/**
 * return the next image to check
 * it could be a camera frame or an image
 * that has has been save in a folder
 * */
cv::Mat Calibrator::nextImage()
{
    cv::Mat result;
    if( video.isOpened() )
    {
        cv::Mat view0;
        video >> view0;
        view0.copyTo(result);
    }
    else if( settings.atImageList < (int)settings.imageList.size() )
        result = cv::imread(settings.imageList[settings.atImageList++], cv::IMREAD_COLOR);

    return result;
}

// TODO - move or remove
// lame help output
const char* Calibrator::help(){
    const char* helpMsg = "This is a camera calibration sample. \n Usage: calibration configurationFile \n Near the sample file you'll find the configuration file, which has detailed help of \n how to edit it.  It may be any OpenCV supported file format XML/YAML.";
    std::cout <<  helpMsg << "\n";
    return helpMsg;
}

// given a frame, use the camera matrix and calculated coeffs to undistort the image
cv::Mat Calibrator:: undistortFrame(cv::Mat& frame){
    logger.Info("Attempting to undistort a frame.");
    cv::Mat uFrame;

    /*
    // -----------------------Show the undistorted image for the image list ------------------------
    if( settings.inputType == Settings::IMAGE_LIST && settings.showUndistorsed )
    {
        cv::Mat view, rview, map1, map2;
        initUndistortRectifyMap(cameraMatrix, distCoeffs, cv::Mat(),
            getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, 0),
            imageSize, CV_16SC2, map1, map2);

        for(int i = 0; i < (int)settings.imageList.size(); i++ )
        {
            view = cv::imread(settings.imageList[i], 1);
            if(view.empty())
                continue;
            remap(view, rview, map1, map2, cv::INTER_LINEAR);
            imshow("Image View", rview);
            char c = (char)cv::waitKey();
            if( c  == ESC_KEY || c == 'q' || c == 'Q' )
                break;
        }
    }
    */

    if(m_cameraMatrix.empty() || m_distCoeffs.empty()){
        logger.Warning("The Camera Matrix and/or Distance Coefficients are empty, did you calibrate?");
        return uFrame;
    }

    try{
        // this is basically what the commented block above does
        cv::undistort(frame, uFrame, m_cameraMatrix, m_distCoeffs);
    }catch(std::exception& err){
        std::cerr << err.what() << "\n";
        logger.Error("Exception when undistorting a frame");
    }
    return uFrame;
}

// returns the error calculated based on the image points to object points
double Calibrator::computeReprojectionErrors(const std::vector<std::vector<cv::Point3f> >& objectPoints,
                                         const std::vector<std::vector<cv::Point2f> >& imagePoints,
                                         const std::vector<cv::Mat>& rvecs, const std::vector<cv::Mat>& tvecs,
                                         const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs,
                                         std::vector<float>& perViewErrors)
{
    logger.Info("Attempting to compute reprojection errors...");
    std::vector<cv::Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for( i = 0; i < (int)objectPoints.size(); ++i )
    {
        cv::projectPoints(cv::Mat(objectPoints[i]), rvecs[i],
                          tvecs[i], cameraMatrix,
                          distCoeffs, imagePoints2);
        err = norm(cv::Mat(imagePoints[i]),
                   cv::Mat(imagePoints2),
                   cv::NORM_L2);

        int n = (int)objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err*err/n);
        totalErr        += err*err;
        totalPoints     += n;
    }

    logger.Info("Done computing reprojection errors.");

    return std::sqrt(totalErr/totalPoints);
}

// populates the corners vector with some more accurate positions
void Calibrator::calcBoardCornerPositions(cv::Size boardSize, float squareSize,
                                          std::vector<cv::Point3f>& corners,
                                          Settings::Pattern patternType /*= Settings::CHESSBOARD*/)
{
    logger.Info("Calculating board corner positions...");
    corners.clear();
    switch(patternType)
    {
    case Settings::CHESSBOARD:
    case Settings::CIRCLES_GRID:
        for( int i = 0; i < boardSize.height; ++i )
            for( int j = 0; j < boardSize.width; ++j )
                corners.push_back(cv::Point3f(float( j*squareSize ), float( i*squareSize ), 0));
        break;

    case Settings::ASYMMETRIC_CIRCLES_GRID:
        for( int i = 0; i < boardSize.height; i++ )
            for( int j = 0; j < boardSize.width; j++ )
                corners.push_back(cv::Point3f(float((2*j + i % 2)*squareSize), float(i*squareSize), 0));
        break;
    default:
        break;
    }
    logger.Info("Done Calculating board corner positions.");
}

// get more accurate corners and find the error between the calibration and reprojection
bool Calibrator::runCalibration(Settings& settings, cv::Size& imageSize, cv::Mat& cameraMatrix,
                                cv::Mat& distCoeffs, std::vector<std::vector<cv::Point2f>> imagePoints,
                                std::vector<cv::Mat>& rvecs, std::vector<cv::Mat>& tvecs,
                                std::vector<float>& reprojErrs,  double& totalAvgErr)
{

    logger.Info("🤖 Attempting to run calibration.");
    try
    {
        cameraMatrix = cv::Mat::eye(3, 3, CV_64F);
        if( settings.flag & cv::CALIB_FIX_ASPECT_RATIO )
            cameraMatrix.at<double>(0,0) = 1.0;

        distCoeffs = cv::Mat::zeros(8, 1, CV_64F);

        std::vector<std::vector<cv::Point3f> > objectPoints(1);
        calcBoardCornerPositions(settings.boardSize,
                                 settings.squareSize,
                                 objectPoints[0],
                                 settings.calibrationPattern);

        objectPoints.resize(imagePoints.size(),objectPoints[0]);

        //Find intrinsic and extrinsic camera parameters
        double rms = cv::calibrateCamera(objectPoints, imagePoints, imageSize,
                                         cameraMatrix, distCoeffs, rvecs, tvecs,
                                         settings.flag| cv::CALIB_FIX_K4 | cv::CALIB_FIX_K5);

        logger.Info("Re-projection error reported by calibrateCamera: " + std::to_string(rms));

        bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

        totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                                rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

        logger.Info("Calibration complete");
        return ok;
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        logger.Error("An error occured when trying to run calibration on the data.");
        throw(e);
    }
    return false;
}

// runCalibrationAndSave
bool Calibrator::runCalibrationAndSave(Settings& settings, cv::Size imageSize,
                                       cv::Mat&  cameraMatrix, cv::Mat& distCoeffs,
                                       std::vector<std::vector<cv::Point2f> > imagePoints)
{
    logger.Info("Running Calibration and Save");
    std::vector<cv::Mat> rvecs, tvecs;
    std::vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(settings, imageSize, cameraMatrix, distCoeffs, imagePoints, rvecs, tvecs,
                             reprojErrs, totalAvgErr);

    std::string m = std::string(ok ? "Calibration succeeded ✨🌈✨" : "Calibration failed 🌧️") + ". avg re projection error = " + std::to_string(totalAvgErr);
    logger.Info(m);

    if(ok)
        saveCameraParams(settings, imageSize, cameraMatrix, distCoeffs, rvecs ,tvecs, reprojErrs,
                            imagePoints, totalAvgErr);
    return ok;
}

// call by ui via thread
void Calibrator::handleCalibration()
{
    std::cout << "iamge size" << m_imageSize.height << " " << m_imageSize.width  << " image points: " << m_imagePoints.size() << "\n";
    try{
        runCalibrationAndSave(settings, m_imageSize, m_cameraMatrix, m_distCoeffs, m_imagePoints);
    }
    catch(std::exception &e)
    {
        throw(e);
    }
}

bool Calibrator::readSettings(std::string const &settingsFilename){
    logger.Info("Reading Settings File");
    // Read the settings
    try {
        cv::FileStorage fs(settingsFilename, cv::FileStorage::READ);
        if (!fs.isOpened())
        {
            logger.Error("Could not open the configuration file: " + settingsFilename);
            return false;
        }

        fs["Settings"] >> settings;
        // close Settings file
        fs.release();

        if (!settings.goodInput)
        {
            logger.Error("Invalid input detected. Application stopping.");
            return false;
        }
    }
    catch(cv::Exception err){
        logger.Warning("oh pooopy... " + err.msg);
    }

    return true;
}

bool Calibrator::writeSettings(std::string const &settingsFilename){
    logger.Info("Attempting to write settings");
    try
    {
        cv::FileStorage fs(settingsFilename, cv::FileStorage::WRITE);
        if (!fs.isOpened())
        {
            logger.Error("Could not open the configuration file: " + settingsFilename);
            return false;
        }
        settings.write(fs);
        // close Settings file
        fs.release();
        logger.Info("Successfully wrote settings to file: " + settingsFilename);
    }
    catch(cv::Exception err) {
        logger.Warning("ahhh suck!");
        std::cout << err.msg << "\n";
    }

    return true;
}

// format the calibrated camera parameter and save to a file
void Calibrator::saveCameraParams(Settings& settings, cv::Size& imageSize, cv::Mat& cameraMatrix, cv::Mat& distCoeffs,
                              const std::vector<cv::Mat>& rvecs, const std::vector<cv::Mat>& tvecs,
                              const std::vector<float>& reprojErrs, const std::vector<std::vector<cv::Point2f> >& imagePoints,
                              double totalAvgErr )
{
    logger.Info("Attempting to save Camera Params to: " + settings.outputFileName);
    try
    {
        cv::FileStorage fs( settings.outputFileName, cv::FileStorage::WRITE );

        time_t tm;
        time( &tm );
        struct tm *t2 = localtime( &tm );
        char buf[1024];
        strftime( buf, sizeof(buf)-1, "%c", t2 );

        fs << "calibration_Time" << buf;

        if( !rvecs.empty() || !reprojErrs.empty() )
            fs << "nrOfFrames" << (int)std::max(rvecs.size(), reprojErrs.size());
        fs << "image_Width" << imageSize.width;
        fs << "image_Height" << imageSize.height;
        fs << "board_Width" << settings.boardSize.width;
        fs << "board_Height" << settings.boardSize.height;
        fs << "square_Size" << settings.squareSize;

        if( settings.flag & cv::CALIB_FIX_ASPECT_RATIO )
            fs << "FixAspectRatio" << settings.aspectRatio;

        if( settings.flag )
        {
            sprintf( buf, "flags: %s%s%s%s",
                settings.flag & cv::CALIB_USE_INTRINSIC_GUESS ? " +use_intrinsic_guess" : "",
                settings.flag & cv::CALIB_FIX_ASPECT_RATIO ? " +fix_aspectRatio" : "",
                settings.flag & cv::CALIB_FIX_PRINCIPAL_POINT ? " +fix_principal_point" : "",
                settings.flag & cv::CALIB_ZERO_TANGENT_DIST ? " +zero_tangent_dist" : "" );
            //cvWriteComment( *fs, buf, 0 );
            fs.writeComment(buf, 0);

        }

        fs << "flagValue" << settings.flag;

        fs << "Camera_Matrix" << cameraMatrix;
        fs << "Distortion_Coefficients" << distCoeffs;

        fs << "Avg_Reprojection_Error" << totalAvgErr;
        if( !reprojErrs.empty() )
            fs << "Per_View_Reprojection_Errors" << cv::Mat(reprojErrs);

        if( !rvecs.empty() && !tvecs.empty() )
        {
            CV_Assert(rvecs[0].type() == tvecs[0].type());
            cv::Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
            for( int i = 0; i < (int)rvecs.size(); i++ )
            {
                cv::Mat r = bigmat(cv::Range(i, i+1), cv::Range(0,3));
                cv::Mat t = bigmat(cv::Range(i, i+1), cv::Range(3,6));

                CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
                CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
                //*.t() is MatExpr (not cv::Mat) so we can use assignment operator
                r = rvecs[i].t();
                t = tvecs[i].t();
            }
            //cvWriteComment( *fs, "a set of 6-tuples (rotation std::vector + translation std::vector) for each view", 0 );
            fs.writeComment("a set of 6-tuples (rotation std::vector + translation std::vector) for each view", 0);
            fs << "Extrinsic_Parameters" << bigmat;
        }

        if( !imagePoints.empty() )
        {
            cv::Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
            for( int i = 0; i < (int)imagePoints.size(); i++ )
            {
                cv::Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
                cv::Mat imgpti(imagePoints[i]);
                imgpti.copyTo(r);
            }
            fs << "Image_points" << imagePtMat;
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << "\n";
        logger.Error("An error occured when saving the calibration data. ");
        throw(e);
    }
}

// TODO(station) - perhaps finish gathering the other params from the file? for now this is all we needed
// read in the camera matrix and distance coefficients from previous calibration
bool Calibrator::readCameraParams(Settings& settings)
{
    logger.Info("Reading in camera parameters");
    try
    {
        //cv::Mat m_extrinsics;
        cv::FileStorage fs(settings.outputFileName, cv::FileStorage::READ);
        fs["Camera_Matrix"] >> m_cameraMatrix;
        fs["Distortion_Coefficients"] >> m_distCoeffs;
        //fs["Extrinsic_Parameters"] >> m_extrinsics;
        fs.release();
    }catch(cv::Exception& exc)
    {
        std::cerr << exc.msg << "\n";
        logger.Error("Error when attempting to read camera params file. "+ exc.msg + " Does "+ settings.outputFileName +" Exist?");
        return false;
    }
    logger.Info("Done reading in camera params");
    //std::cout << m_cameraMatrix << "\n";
    //std::cout << m_distCoeffs << "\n";
    return true;
}

// simple wrapper for when called via UI callback
bool Calibrator::readCameraParams()
{
    return readCameraParams(settings);
}

// TODO(Station) - change file uri to be set in the GUI
// saves a mat to disk as jpeg in home dir
Calibrator::DetectionResult Calibrator::saveCapturedImage(cv::Mat& frame)
{
    logger.Info("Attempting to save out a frame...");
    Calibrator::DetectionResult result;
    using namespace std::chrono;

    milliseconds ms = duration_cast< milliseconds >(system_clock::now().time_since_epoch());
    std::string fn =  calibrationImageDirectory + settings.outputImageFileName + "_" + std::to_string(ms.count()) + ".jpg";
    try
    {
        bool res = cv::imwrite(fn, frame);
        result = Calibrator::DetectionResult{found: res, filepath: fn};
        logger.Info("Saved image to: "+ fn);
    }
    catch(cv::Exception err) {
        logger.Error("Failed to save frame as image to disk: "+fn);
        std::cerr << err.msg << "\n";
        result = Calibrator::DetectionResult{found: false, filepath: fn};
    }
    return result;
}

// find the cameras listed in /dev and return the string list
// Linux only support for now
std::vector<std::string> Calibrator::getCameraPaths() 
{
    std::vector<std::string> cameras = {};

    std::string path = "/dev";
    for (const auto & entry : fs::directory_iterator(path))
    {
        std::cout << entry.path() << std::endl;
        if(entry.path().string().find("video") != std::string::npos) {
            cameras.push_back(entry.path().string());
        }
    }

    return cameras;
}

