#include <QtCore/QtCore>

#include "calibrator.h"

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

class Worker : public QObject
{
    Q_OBJECT
public:
    Worker();
    ~Worker();

    bool isAlive;
    //QProcess *proc;

public slots:
    void readVideo();
    void toggleCamera(QString devicepath);
    void toggleCapture();
    void runCalibration();
    void undistortFrame();

signals:
    // frame and index of label which frame will be displayed
    void frameFinished(cv::Mat frame);
    void finished();
    void checkerDetected(QString filepath);
    void calibrationFinished(QString msg);
    void undistortFinished(cv::Mat frame);

private:
    QString filepath;
    Calibrator calibrator;
};