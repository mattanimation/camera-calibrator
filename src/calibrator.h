#pragma once

#include <iostream>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <map>

#include <filesystem>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/persistence.hpp>

#include "settings.h"
#include "logger.h"

namespace fs = std::filesystem;

enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

class Calibrator
{
public:
    Calibrator();
    ~Calibrator();

    struct DetectionResult {
        bool found;
        std::string filepath;
    };

    bool readSettings(std::string const &settingsFilename);
    bool writeSettings(std::string const &settingsFilename);

    void calcBoardCornerPositions(cv::Size boardSize, float squareSize, std::vector<cv::Point3f>& corners,
                                     Settings::Pattern patternType /*= Settings::CHESSBOARD*/);

    bool runCalibration(Settings& s, cv::Size& imageSize, cv::Mat& cameraMatrix, cv::Mat& distCoeffs,
                        std::vector<std::vector<cv::Point2f> > imagePoints, std::vector<cv::Mat>& rvecs, std::vector<cv::Mat>& tvecs,
                        std::vector<float>& reprojErrs,  double& totalAvgErr);

    void saveCameraParams( Settings& s, cv::Size& imageSize, cv::Mat& cameraMatrix, cv::Mat& distCoeffs,
                              const std::vector<cv::Mat>& rvecs, const std::vector<cv::Mat>& tvecs,
                              const std::vector<float>& reprojErrs, const std::vector<std::vector<cv::Point2f> >& imagePoints,
                              double totalAvgErr );
    bool readCameraParams(Settings& s);
    bool readCameraParams();
    bool runCalibrationAndSave(Settings& s, cv::Size imageSize, cv::Mat&  cameraMatrix,
                               cv::Mat& distCoeffs,std::vector<std::vector<cv::Point2f> > imagePoints );
    void handleCalibration();

    bool startCamera(std::string devicepath);
    void stopCamera();

    void startCapture();
    void stopCapture();

    static std::vector<std::string> getCameraPaths();

    bool startCaptureOrProcessImages();
    void stopCaptureOrProcessImages();
    DetectionResult processFrame(cv::Mat& frame);
    cv::Mat undistortFrame(cv::Mat& frame);

    cv::Mat getCameraFrame();

    Settings settings;
    bool isActive;
    bool isCapturing;
    const char* help();

private:
    double computeReprojectionErrors(const std::vector<std::vector<cv::Point3f> >& objectPoints,
                                     const std::vector<std::vector<cv::Point2f> >& imagePoints,
                                     const std::vector<cv::Mat>& rvecs, const std::vector<cv::Mat>& tvecs,
                                     const cv::Mat& cameraMatrix , const cv::Mat& distCoeffs,
                                     std::vector<float>& perViewErrors);

    DetectionResult saveCapturedImage(cv::Mat& frame);
    char *calibrationImageDirectory;

    std::chrono::milliseconds lastFoundTime;
    int64_t foundWaitTime = 3000;

    cv::Mat nextImage();
    cv::Mat m_greyFrame;
    cv::Mat m_drawFrame;
    cv::VideoCapture video;
    Logger logger;
    std::string m_version = "0.0.1";
    std::vector<std::vector<cv::Point2f>> m_imagePoints;
    cv::Mat m_cameraMatrix, m_distCoeffs;
    cv::Size m_imageSize;

};