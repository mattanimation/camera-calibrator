# How to Use the Calibrator

## Pre-requrisites
* checker board printed on paper or foam core board
* know the size of checker board
* have the `default.xml` config file in the same directory as the binary to launch

Download a checkerboard to print from [here](https://markhedleyjones.com/projects/calibration-checkerboard-collection)

## Calibration Steps
1. open the config file and change any parameters like `checkerboard size` (remember inner count of square not outside), or `camera device`
2. start tool
![main window](assets/calib_01.png)
3. click `start camera`, make sure you get a feel for moveing the checkerboard around
4. click `start capture` - you will see the detection flash every 3 seconds if it can find good patterns
5. checkerboard around, ideally covering the edge of cameras
![images captured](assets/calib_02.png)
6. when you have around 20 or so you can click `stop capture` 
7. click `calibrate` - now it that was a success you will see a pop-up that tells you where the calibrated values are
![undistorted window](assets/undistort_example.png)
8. to test the offsets, keep the camera on (or turn it on if it was off), make sure a camera checkerboard is in frame and click `undistort` - if all is well you should see a pop-up window that has an undistorted version of the camera frame that was captured.


